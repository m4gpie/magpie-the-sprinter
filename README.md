# Magpie the Sprinter

This repo documents my van conversion done during the COVID-19 crisis in Brighton, UK.

In March 2020, just before lockdown, I purchased a 2006 Mercedes Sprinter 311 CDI with a long wheel base and a high roof. It was an ex-NHS diabeties trust vehicle and has very low mileage for a sprinter, but has had steady regular use, so seemed a good fit for the job.

<p align="center">
  <img src="./assets/magpie-01.jpg" alt="magpie-the-sprinter" width="600">
</p>

You can find my [development diary](./diary) to learn more about the process I've gone through!

## Useful Links

* [General things to think about](https://mpora.com/outsiders/10-things-you-need-to-know-before-buying-a-van-to-travel-the-world#2YhzvZ69rdBzSS7v.97)
* [Insulation](https://www.vanlifeadventure.com/campervan-conversion/campervan-insulation/)
* [Cool Conversions](https://quirkycampers.co.uk/)
* [MOT history of a vehicle](https://www.check-mot.service.gov.uk/)
* [Charging a Leisure Battery](https://www.campingandcaravanningclub.co.uk/helpandadvice/technicalhelp/datasheets/charging-leisure-battery/page1/#Types)
* [UK Gov't conversion information](https://www.gov.uk/government/publications/converting-a-vehicle-into-a-motor-caravan/converting-a-vehicle-into-a-motor-caravan)
* [Gov't conversion checklist](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/840319/v1006-motor-caravan-conversion-checklist.pdf)
* [Van Dimensions](https://vandimensions.com/database)
* [Calculating Heat Loss](https://www.youtube.com/watch?v=hDZBJw7cV2U&list=PLLhAyWhGGbi6JLJBHePgsmR-jSbyC_RgO&index=12)
* [Heating](https://www.quirkycampers.co.uk/campervan-heating-options)

## Worksheets

* [Budget](./assets/budget.csv)
* [Electrics](./assets/electrics.csv)
* [Dimensions and Layout](./assets/dimensions.csv)

## Floorplan

![floorplan](./assets/floorplan.png)

## Interior Dimensions

![interior-dimensions](./assets/interior-dimensions.png)

## Index

* [Features](#features)
* [Services](#services)
* [Insulation](#insulation)
* [Ventilation](#ventilation)
* [Moisture Protection](#moisture-protection)
* [Water](#water)
* [Heating](#heating)
* [Electrics](#electrics)
* [Kitchen](#kitchen)
* [Safety / Security](#safety-security)

## Features
* Toilet and Shower Room
* Kitchen
* Kitchen Storage (below, above)
* Raised Double Bed
* Overbed cupboards for storage
* Garage area
* Fold out table and chairs (converts to additional bed)
* Wood Burner Heating
* Boiler Heating
* Solar Panels
* 12V and 230/240V sockets
* 240V hook up to charge leisure batteries
* Rear-view camera
* Roof fan for ventilation

## Services
* Hot and Cold Water
* 12V and 230/240V electrics
* Gas

## Insulation

Thermal conductivity is measured in W/mK (λ - watts per meter-kelvin), the amount of watts able to pass through a material. This will be indicated on all insulation materials you can buy. We want to aim for a very low thermal conductivity, while ideally using organic / sustainable materials. Heat Loss Calculation will tell us help much heat will be lost through the thickness of the insulated wall.

`Resistance (R) = m/λ`

Mineral Earthwool can be used to fill cavities. Thermaboard can be used for the floor, roof and side panels.

`U-Value = sum(R)`

`Temperature difference = target internal temperature - external temperature`

`Heat Loss (w) = Area x U-Value x TempDiff`

Sum heat loss for cab + cabin = total heat loss

### Products
* [PIR](https://www.ecotherm.co.uk/products/eco-cavity-full-fill)
* Earthwool (organic) - 0.037 W/mK
* Thermaboard (PIR - Polyisocyanurate)  - 0.022 W/mK

### Tips
* Stick pins can be used to fix thermaboard securely to the roof
* Raise the floor using 2x2 and ensure its solid and insulate inbetween with PIC

## Ventilation
Use of a roof fans or air conditioning unit will ensure the van remains properly ventilated which helps remove moisture and minimise condenstation inside the van.

## Moisture Protection

We want to minimise the amount of warm moist air inside condensing on the cold steel, so as to prevent rot setting in in organic insulation and excess exposure of moisture to the metal which can cause rust.

### Products
* [Foil-faced bubble wrap]()
  * Known as  'reflectics'
  * Covers the entire inside of the cabin after putting in insulation.
  * Reflects infrared light so will assist (only a small amount) with insulation layer. Cannot be used as primary insulation, its _not an insulator_.
* [Foil tape]()
  * Seals any gaps

## Water

We want two water storage tanks, one for grey water and one for shower water. We'll also need an additional smaller water storage tank for drinking water. A 12V pump can run from the main water tank attached under the body of the vehicle and pump water up to the boiler and be redirected to the main sink tap and the shower head. A 12V pump can run from inside the drinking water tank up to the sink to a secondary smaller drinking water tap.

## Heating

We want a wood burner for when wood is available and aesthetically is awesome. But also a gas heater is a great main option (for hot water and cirulating hot air), which can be used some days.

### Product
* [Louis Wood Burner](http://www.windysmithy.co.uk/woodburners/louis)
  * 4 KW max output
  * £310 + £25 delivery
  * 440h x 405w x 500d
  * [Heating water with wood burner](http://solarhomestead.com/using-your-wood-stove-to-heat-water/)
* Gas Truma Combi Boiler

## Electrics
* Leisure Batteries
* 150w Solar panel on roof
* External power port to run off / charge at homes / campsites
* Charge leisure batteries while running the engine
* 12v DC for most appliances
* 230/240v for typical mains-adapter appliances
* Fridge
  * 2-way compressor fridge/freezer, runs of 12v DC or 230/240v AC
  * Power Consumption between 50w-75w
  * Typically runs off a single 120w solar panel
* Trips / Fuses + Fusebox
* Lighting, LEDs, granular if possible so we don't have to light the entire interior
* 12v submersible pump for drinking water supply

### Links
* Solar Panel: https://www.ebay.co.uk/p/Photonic-Universe-SPA-150M-160W-12V-Monocrystalline-Solar-Panel/15031188125
* Fridge: https://www.quirkycampers.co.uk/campervan-fridges/ - article recommends a 2-way compressor fridge which can be (relatively) reliably run off a 120v solar panel

## Kitchen

* Propane gas cylinder with hob
* Plastic / Bamboo crockery
* Kettle
    * Surely gas makes most sense in terms of energy efficiency, but its worth calculating the energy requirements of an insulated smart kettle to compare (https://www.amazon.co.uk/dp/B07VC9Q318/ref=psdc_3538311031_t1_B07HN8WJXJ)
* Fridge / Freezer is a must have, particularly a freezer will be useful for storing cooked food
* Air-tight containers for dried food (e.g. storing dried mushrooms) (kilner jars)
* Drinking water store - 10L (with 12v submersible pump)
* A wood burner doubles up as a cooking plate

### Tips
* Dipped cupboards to hold things in place when the vehicle is moving

## Safety / Security
* Locks
* Carbon monoxide alarm
* Mini safe for valuables
* Fire Extinguisher

## Product Links

- [2001 - 2006 Sprinter 3 piece cab window insulation set](https://www.eurocampers.com/2001--2006-Sprinter-3-piece-Cab-Window-Insulation-Set--advanced-7-layer-material-uses-suction-cups-_p_32.html)
- [Outwell Windscreen 5m / 1.4m](https://www.outdoormegastore.co.uk/outwell-windscreen-treetop-green.html)
